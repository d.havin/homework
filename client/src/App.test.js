import React from "react";
import ReactDom from 'react-dom';
// import App from "./App";
import AppGeneral from "./App"

it('renders without crashing', () => {
    const div = document.createElement('div');          // создаем дивку
    ReactDom.render(<AppGeneral/>, div);                // рендерим туда апп
    ReactDom.unmountComponentAtNode(div);               // чистим
})