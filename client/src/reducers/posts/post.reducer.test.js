import postReducer from "./post.reducer";
import { getAllPostsRequest, getAllPostsRequestFailed, getAllMyPostsRequest, getAllMyPostsRequestFailed, getAllMyFriendsPostRequest, getAllMyFriendsPostRequestFailed,
    addPostRequest, addPostRequestFailed } from "../../actionCreator/user.action"
import { GET_ALL_POSTS_REQUEST_SUCCESS, GET_ALL_MY_POSTS_REQUEST_SUCCESS, GET_ALL_MY_FRIENDS_POSTS_REQUEST_SUCCESS, REQUEST_ADD_POST_SUCCESS, REQUEST_DELETE_POST_SUCCESS, 
    LIKE_POST_SUCCESS } from "../../constants/constants"

let state = {
    loading : false,
    error: false,
    allposts: [],
    friendsPosts: [],
    myposts: []
}

describe('Post Reducer', () => {

    it('Should return default state', () => {
        let action = "no match with real action"
        let newState = postReducer(state, action)
        expect(newState).toEqual(state)
    });

    it('Get all posts loader should be changed', () => {
        let action = getAllPostsRequest()
        let newState = postReducer(state, action)
        expect(newState.loading).toBe(true)
        expect(newState.error).toBe(false)
    })

    it('allposts length should be incremented', () => {
        let action = {
            type: GET_ALL_POSTS_REQUEST_SUCCESS,
            data: [1,2,3]
        }
        let newState = postReducer(state, action)
        expect(newState.loading).toBe(false)
        expect(newState.error).toBe(false)
        expect(newState.allposts.length).toBe(3)
    })

    it('Get all posts error should be "true"', () => {
        let action = getAllPostsRequestFailed()
        let newState = postReducer(state, action)
        expect(newState.loading).toBe(false)
        expect(newState.error).toBe(true)
        expect(newState.allposts.length).toBe(0)
    }) 

    it('Get allMyPosts loader should be changed', () => {
        let action = getAllMyPostsRequest()
        let newState = postReducer(state, action)
        expect(newState.loading).toBe(true)
        expect(newState.error).toBe(false)
    })

    it('Get allMyposts length should be incremented', () => {
        let action = {
            type: GET_ALL_MY_POSTS_REQUEST_SUCCESS,
            data: [1,2,3,4]
        }
        let newState = postReducer(state, action)
        expect(newState.loading).toBe(false)
        expect(newState.error).toBe(false)
        expect(newState.myposts.length).toBe(4)
    })

    it('Get all posts error should be "true"', () => {
        let action = getAllMyPostsRequestFailed()
        let newState = postReducer(state, action)
        expect(newState.loading).toBe(false)
        expect(newState.error).toBe(true)
        expect(newState.myposts.length).toBe(0)
    }) 

    it('Get allMyFriendsPosts loader should be changed', () => {
        let action = getAllMyFriendsPostRequest()
        let newState = postReducer(state, action)
        expect(newState.loading).toBe(true)
        expect(newState.error).toBe(false)
    })

    it('Get allMyFriendsPosts length should be incremented', () => {
        let action = {
            type: GET_ALL_MY_FRIENDS_POSTS_REQUEST_SUCCESS,
            data: [1,2,3,4]
        }
        let newState = postReducer(state, action)
        expect(newState.loading).toBe(false)
        expect(newState.error).toBe(false)
        expect(newState.friendsPosts.length).toBe(4)
    })

    it('Get allMyFriendsPosts error should be "true"', () => {
        let action = getAllMyFriendsPostRequestFailed()
        let newState = postReducer(state, action)
        expect(newState.loading).toBe(false)
        expect(newState.error).toBe(true)
        expect(newState.friendsPosts.length).toBe(0)
    }) 

    it('Request add post', () => {
        let action = addPostRequest()
        let newState = postReducer(state, action)
        expect(newState.loading).toBe(true)
        expect(newState.error).toBe(false)
    })

    it('Add post success - myposts length should be incremented', () => {
        let action = {
            type: REQUEST_ADD_POST_SUCCESS,
            data: [1]
        }
        let newState = postReducer(state, action)
        expect(newState.loading).toBe(false)
        expect(newState.error).toBe(false)
        expect(newState.myposts.length).toBe(1)
    })

    it('Add post error', () => {
        let action = addPostRequestFailed()
        let newState = postReducer(state, action)
        expect(newState.loading).toBe(false)
        expect(newState.error).toBe(true)
    })

    it('Delete post success', () => {
        let action = {
            type: REQUEST_DELETE_POST_SUCCESS,
            data: [1,2]
        }
        let newState = postReducer(state, action)
        expect(newState.myposts.length).toBe(2)
    })

    it('Like post', () => {
        let action = {
            type: LIKE_POST_SUCCESS,
            data: [ 1,2 ],
            data1: [ 1, 2, 3 ],
            data2: [ 4 ]
        }
        let newState = postReducer(state, action)
        expect(newState.allposts.length).toBe(2)
        expect(newState.myposts.length).toBe(1)
        expect(newState.friendsPosts.length).toBe(3)
    })

});






























// test('данные являются арахисовым маслом', done => {
//     function callback(data) {
//       try {
//         expect(data).tobe('арахисовое масло');
//         done();
//       } catch (error) {
//         done(error);
//       }
//     }
//     fetchGetAllPosts(callback);
// });


// test('the data is peanut butter', () => {
//     return fetchData().then(data => {
//       expect(data).toBe('peanut butter');
//     });
// });


// export const addPostRequest = () => ({type: REQUEST_ADD_POST});
// export const addPostRequestSuccess = (data) => ({type: REQUEST_ADD_POST_SUCCESS, data});
// export const addPostRequestFailed = () => ({type: REQUEST_ADD_POST_FAILED});
// export const fetchAddPost = (data) => ({type: FETCH_ADD_POST, data});






