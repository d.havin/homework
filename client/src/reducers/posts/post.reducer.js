import { GET_ALL_POSTS_REQUEST, GET_ALL_POSTS_REQUEST_SUCCESS, GET_ALL_POSTS_REQUEST_FAILED,
    GET_ALL_MY_FRIENDS_POSTS_REQUEST, GET_ALL_MY_FRIENDS_POSTS_REQUEST_SUCCESS, GET_ALL_MY_FRIENDS_POSTS_REQUEST_FAILED,
    GET_ALL_MY_POSTS_REQUEST, GET_ALL_MY_POSTS_REQUEST_SUCCESS, GET_ALL_MY_POSTS_REQUEST_FAILED,

    REQUEST_ADD_POST, REQUEST_ADD_POST_SUCCESS, REQUEST_ADD_POST_FAILED,
    REQUEST_DELETE_POST, REQUEST_DELETE_POST_SUCCESS, REQUEST_DELETE_POST_FAILED,
    LIKE_POST_SUCCESS } from '../../constants/constants'

const defaultState = {
    loading : false,
    error: false,
    allposts: [],
    friendsPosts: [],
    myposts: []
}

export default function postReducer (state = defaultState, action){
    switch (action.type) {

        case GET_ALL_POSTS_REQUEST:
            return {
                error: false,
                loading : true,
                allposts: []
            }
        case GET_ALL_POSTS_REQUEST_SUCCESS:
            return {
                error: false,
                loading : false,
                allposts: action.data
            }
        case GET_ALL_POSTS_REQUEST_FAILED:
            return {
                loading : false,
                error: true,
                allposts: []
            }

        case GET_ALL_MY_FRIENDS_POSTS_REQUEST:
            return {
                error: false,
                loading : true,
                friendsPosts: []
            }
        case GET_ALL_MY_FRIENDS_POSTS_REQUEST_SUCCESS:
            return {
                error: false,
                loading : false,
                friendsPosts: action.data
            }
        case GET_ALL_MY_FRIENDS_POSTS_REQUEST_FAILED:
            return {
                loading : false,
                error: true,
                friendsPosts: []
            }
        
        case GET_ALL_MY_POSTS_REQUEST:
            return {
            loading : true,
            error: false,
            myposts: []
        }
        case GET_ALL_MY_POSTS_REQUEST_SUCCESS:
            return {
            loading : false,
            error: false,
            myposts: action.data
        }
        case GET_ALL_MY_POSTS_REQUEST_FAILED:
            return {
                loading : false,
                error: true,
                myposts: []
            }

        case REQUEST_ADD_POST:
            return {
                ...state,
                loading : true,
        }
        case REQUEST_ADD_POST_SUCCESS:
            return {
                ...state,
                myposts: action.data,
                loading : false, 
        }

        case REQUEST_ADD_POST_FAILED:
            return {
                ...state,
                loading : false,
                error: true
        }

        case REQUEST_DELETE_POST:
            return {
                ...state,
                loading : true,
                error: false
        }
        case REQUEST_DELETE_POST_SUCCESS:
            return {
                myposts: action.data,
                loading : false,
                error: false
        }
        case REQUEST_DELETE_POST_FAILED:
            return {
                ...state,
                loading : false,
                error: true
        }
        case LIKE_POST_SUCCESS: 
            return {
                ...state,
                allposts: action.data,
                friendsPosts: action.data1,
                myposts: action.data2
            }
        default:
            return state;
    }
}

