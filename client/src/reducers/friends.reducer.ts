import { GET_ALL_USERS_REQUEST_SUCCESS, GET_ALL_USERS_REQUEST_FAILED, 
    ADD_FRIEND_REQUEST_SUCCESS, CANCEL_REQUEST_SUCCESS, APPLY_INVITATION_SUCCESS, REJECT_INVITATION_SUCCESS,  
    DELETE_FRIEND_SUCCESS,  
    GET_ALL_MY_INVITATIONS_SUCCESS,
    GET_ALL_MY_REQUESTS_SUCCESS,
    GET_ALL_MY_FRIENDS_SUCCESS, } from '../constants/constants'

const defaultState = {
    loading : false,
    error: false,
    users: [],
    myfriends: [],
    invitations: [],
    requests: []
}

export default function friendReducer (state = defaultState, action : any){
    switch (action.type) {

        case GET_ALL_USERS_REQUEST_SUCCESS:
            return {
                ...state,
                loading : false,
                users: action.data
            }
        case GET_ALL_USERS_REQUEST_FAILED:
            return {
                ...state,
                loading : false,
                error: true,
            }
//====================================================== CRUD friend
        case ADD_FRIEND_REQUEST_SUCCESS:
            return {
                ...state,
                users: action.data,
                requests: action.data1
            }
        case CANCEL_REQUEST_SUCCESS:
            return {
                ...state,
                users: action.data,
                requests: action.data1
            }
        case APPLY_INVITATION_SUCCESS:
            return {
                ...state,
                users: action.data,
                invitations: action.data1,
                myfriends: action.data2
            }
        case REJECT_INVITATION_SUCCESS:
            return {
                ...state,
                users: action.data,
                invitations: action.data1,
            }
        case DELETE_FRIEND_SUCCESS:
            return {
                ...state,
                users: action.data,
                myfriends: action.data1
            }
 //======================================================get Friends page Data
        case GET_ALL_MY_FRIENDS_SUCCESS:
            return {
                ...state,
                myfriends: action.data
            }
        case GET_ALL_MY_INVITATIONS_SUCCESS:
            return {
                ...state,
                invitations: action.data
                    }
        case GET_ALL_MY_REQUESTS_SUCCESS:
            return {
                ...state,
                requests: action.data
            }
        default:
            return state;
    }
}
