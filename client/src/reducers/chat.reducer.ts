import { GET_CHAT_SUCCESS, CREATE_CHAT_SUCCESS} from "../constants/constants";
    
    const defaultState = { 
        chat : {
            messages: [],
            users : [],
            id: "",
            friend: ''
        },
        currentChat: {}
    }

export default function chatReducer (state = defaultState, action : any){
    switch (action.type) {

        case CREATE_CHAT_SUCCESS:
            return {
                chat: action.data
            }
        case GET_CHAT_SUCCESS:
            return {
                currentChat: action.data
            }
        default:
            return state;
    }
}; 
