import authReducer from "./auth.reducer"
import { userRegistrationRequest, userRegistrationRequestSuccess, userRegistrationRequestFailed, userLoginRequest, userLoginRequestSuccess, userLoginRequestFailed, 
    changeAvatarRequest, changeAvatarRequestSuccess, changeAvatarRequestFailed } from "./../actionCreator/user.action"

const state = {
    loading : false,
    error: false,
    message : "",

    logedUser : {},
    isAuth : false,
    token : "", 
}

describe('Auth Reducer', () => {
    it('Should return default state', () => {
        let action = "no match with real action"
        let newState = authReducer(state, action)
        expect(newState).toEqual(state)
        // console.log(state)
    });

    it('Request Registrate user', () => {
        let action = userRegistrationRequest()
        let newState = authReducer(state, action)
        expect(newState).toEqual({
            ...state,
            loading: true
        })
    });

    it('Request Registrate user success', () => {
        let action = userRegistrationRequestSuccess()
        let newState = authReducer(state, action)
        expect(newState).toEqual({
            ...state,
            message: "Successfull. Go to login page",
            loading: false
        })
    })

    it('Request Registrate user failed', () => {
        let action = userRegistrationRequestFailed()
        let newState = authReducer(state, action)
        expect(newState).toEqual({
            ...state,
            message: "Something went wrong, try again",
            error: true
        })
    })

    it('Request login user', () => {
        let action = userLoginRequest()
        let newState = authReducer(state, action)
        expect(newState).toEqual({
            ...state,
            loading: true
        })
    })

    it('Login user success', () => {
        let data = {
            token: "test-token",
            user : "test-user-id",
        }
        let action = userLoginRequestSuccess(data)
        let newState = authReducer(state, action)
        expect(newState).toEqual({
            message: 'Successfull',
            token: 'test-token',
            logedUser: 'test-user-id',
            loading: false,
            isAuth: true
        })
    })

    it('Login user error', () => {
        let data = {
            token: "test-token",
            user : "test-user-id",
        }
        let action = userLoginRequestFailed(data)
        let newState = authReducer(state, action)
        expect(newState).toEqual({
            ...state,
            message: "Login or password is incorrect",
            loading: false,
            error: true
        })
    })

    it('Request change avatar', () => {
        let action = changeAvatarRequest()
        let newState = authReducer(state, action)
        expect(newState).toEqual({
            ...state,
            loading: true
        })
    })


    it('Change avatar success', () => {
        let data = { updatedUser: "updated user" }
        let action = changeAvatarRequestSuccess(data)
        let newState = authReducer(state, action)
        expect(newState).toEqual({
            ...state,
            logedUser: "updated user",
            loading: false
        })
    })

    it('Change avatar error', () => {
        let data = { updatedUser: "updated user" }
        let action = changeAvatarRequestFailed (data)
        let newState = authReducer(state, action)
        expect(newState).toEqual({
            ...state,
            loading: false,
            error: true,
            message: "SomeThing went wrong" 
        })
    })
})
