import friendReducer from "./friends.reducer";
import { getAllUsersRequestFailed, getAllUsersRequestSuccess, addFriendRequestSuccess, cancelRequestSuccess, applyInvitation, rejectInvitationSuccess,
    deleteFriendSuccess, getAllMyInvitationsSuccess, getAllMyRequestsSuccess, getAllMyFriendsSuccess } from "../actionCreator/user.action"

const state = {
    loading : false,
    error: false,
    users: [],
    myfriends: [],
    invitations: [],
    requests: []
}

describe('Friend Reducer', () => {
    it('Should return default state', () => {
        let action = "no match with real action"
        let newState = friendReducer(state, action)
        expect(newState).toEqual(state)
    });

    it('Get all users success', () => {
        let data = [{id: 1}, {id: 2}, {id: 3}]
        let action = getAllUsersRequestSuccess(data)
        let newState = friendReducer(state, action)
        expect(newState).toEqual({
            ...state,
            loading: false,
            users: [{id: 1}, {id: 2}, {id: 3}]
        })
    });    

    it('Get all users error', () => {
        let data = [{id: 1}, {id: 2}, {id: 3}]
        let action = getAllUsersRequestFailed(data)
        let newState = friendReducer(state, action)
        expect(newState).toEqual({
            ...state,
            loading: false,
            error: true
        })
    }); 

    it('Add friend request', () => {
        let data = [{id: 1}, {id: 2}, {id: 3}]
        let data1 = [{id: 1}]
        let action = addFriendRequestSuccess(data, data1)
        let newState = friendReducer(state, action)
        expect(newState).toEqual({
            ...state,
            users: [{id: 1}, {id: 2}, {id: 3}],
            requests: [{id: 1}]
        })
    }); 

    it('Add friend cancel request', () => {
        let data = [{id: 1}, {id: 2}, {id: 3}]
        let data1 = [{id: 1}, {id: 2}]
        let action = cancelRequestSuccess(data, data1)
        let newState = friendReducer(state, action)
        expect(newState).toEqual({
            ...state,
            users: [{id: 1}, {id: 2}, {id: 3}],
            requests: [{id: 1}, {id: 2}]
        })
    }); 

    it('Apply invitation', () => {
        let data = [{id: 1}, {id: 2}, {id: 3}]
        let data1 = [{id: 1}]
        let data2 = [{id: 1}, {id: 2}]
        let action = applyInvitation (data, data1, data2)
        let newState = friendReducer(state, action)
        expect(newState).toEqual({
            ...state,
            users: [{id: 1}, {id: 2}, {id: 3}],
            myfriends: [{id: 1}, {id: 2}],
            invitations: [{id: 1}]
        })
    }); 
    
    it('Reject invitation', () => {
        let data = [{id: 1}, {id: 2}, {id: 3}]
        let data1 = [{id: 1}]
        let action = rejectInvitationSuccess (data, data1)
        let newState = friendReducer(state, action)
        expect(newState).toEqual({
            ...state,
            users: [{id: 1}, {id: 2}, {id: 3}],
            invitations: [{id: 1}]
        })
    }); 
    
    it('Delete friend success', () => {
        let data = [{id: 1}, {id: 2}, {id: 3}]
        let data1 = [{id: 1}]
        let action = deleteFriendSuccess (data, data1)
        let newState = friendReducer(state, action)
        expect(newState).toEqual({
            ...state,
            users: [{id: 1}, {id: 2}, {id: 3}],
            myfriends: [{id: 1}]
        })
    });

    it('Get all my friends', () => {
        let data = [{id: 1}, {id: 4}]
        let action = getAllMyFriendsSuccess(data)
        let newState = friendReducer(state, action)
        expect(newState).toEqual({
            ...state,
            myfriends: [{id: 1}, {id: 4}]
        })
    });

    it('Get all my invitations', () => {
        let data = [{id: 2}, {id: 3}]
        let action = getAllMyInvitationsSuccess(data)
        let newState = friendReducer(state, action)
        expect(newState).toEqual({
            ...state,
            invitations: [{id: 2}, {id: 3}]
        })
    }); 

    it('Get all my requests', () => {
        let data = [{id: 1}, {id: 3}]
        let action = getAllMyRequestsSuccess(data)
        let newState = friendReducer(state, action)
        expect(newState).toEqual({
            ...state,
            requests: [{id: 1}, {id: 3}]
        })
    });
})
