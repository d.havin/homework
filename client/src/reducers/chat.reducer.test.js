import chatReducer from "./chat.reducer";
import { createChatSuccsess, getChatSuccess } from "../actionCreator/user.action"

const state = { 
    chat : {
        messages: [],
        users : [],
        id: "",
        friend: ''
    },
    currentChat: {}
}

describe('Chat Reducer', () => {
    it('Should return default state', () => {
        let action = "no match with real action"
        let newState = chatReducer(state, action)
        expect(newState).toEqual(state)
    });

    it('Create chat success', () => {
        let data = {
            messages: [],
            users: ["first user", "second user"],
            id: "id of the chat",
            friend: "second user"
        }
        let action = createChatSuccsess(data)
        let newState = chatReducer(state, action)
        expect(newState).toEqual({
            chat:{
                messages: [],
                users: ["first user", "second user"],
                id: "id of the chat",
                friend: "second user" 
            }
        })
    });

    it('Get chat success', () => {
        let data = 
            "current chat"
        let action = getChatSuccess(data)
        let newState = chatReducer(state, action)
        expect(newState).toEqual({currentChat: "current chat"})
    });
})


