import { REQUEST_REGISTRATE_USER, REQUEST_REGISTRATE_USER_SUCCESS, REQUEST_REGISTRATE_USER_FAILED, 
    REQUEST_LOGIN_USER, REQUEST_LOGIN_USER_SUCCESS, REQUEST_LOGIN_USER_FAILED, 
    REQUEST_CHANGE_AVATAR, REQUEST_CHANGE_AVATAR_SUCCESS, REQUEST_CHANGE_AVATAR_FAILED
} from "../constants/constants";

const defaultState = {
    loading : false,
    error: false,
    message : "",

    logedUser : {},
    isAuth : false,
    token : "", 
}

export default function authReducer (state = defaultState, action : any){
    switch (action.type) {

        case REQUEST_REGISTRATE_USER:
            return {
                ...state,
                loading : true,
            }
        case REQUEST_REGISTRATE_USER_SUCCESS:
            return {
                ...state,
                message: "Successfull. Go to login page",
                loading : false,
            }
        case REQUEST_REGISTRATE_USER_FAILED:
            return {
                ...state,
                loading: false,
                message: "Something went wrong, try again",
                error: true,
            }
        case REQUEST_LOGIN_USER:
            return {
                ...state,
                loading : true
            }
        case REQUEST_LOGIN_USER_SUCCESS:
            return {
                message: "Successfull",
                token: action.data.token,
                logedUser : action.data.user,
                loading: false,
                isAuth : true
            }; 

        case REQUEST_LOGIN_USER_FAILED:
            return {
                ...state,
                loading: false,
                error: true,
                message: "Login or password is incorrect"
            }; 
//
        case REQUEST_CHANGE_AVATAR:
            return {
                ...state,
                loading : true,
            }
        case REQUEST_CHANGE_AVATAR_SUCCESS:
            return {
                ...state, 
                loading : false,
                logedUser: action.data.updatedUser
            }
        case REQUEST_CHANGE_AVATAR_FAILED:
            return {
                ...state,
                error: true,
                loading: false,
                message: "SomeThing went wrong"
            }

        default:
            return state;
    }
}
