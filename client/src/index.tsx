import ReactDOM from 'react-dom';
import AppGeneral from './App';
// import { Provider } from 'react-redux';
// import { store } from "./store/store";

ReactDOM.render(
    <AppGeneral />
    ,
    document.getElementById('root')
);