import './navBar.styles.css'
import { NavLink } from "react-router-dom";

const NavBar = () => {
    return (
        <nav className = "navbarblock">
            <NavLink className = "navigation" to = "/payment"><p className = "menuItem">Armory</p></NavLink>
            <NavLink className = "navigation" to = "/profile"><p className = "menuItem">Profile</p></NavLink>
            <NavLink className = "navigation" to = "/myfriends"><p className = "menuItem">My friends</p></NavLink>
            <NavLink className = "navigation" to = "/findfriends"><p className = "menuItem">Find new friends</p></NavLink>
            <NavLink className = "navigation" to = "/messages"><p className = "menuItem">Messages</p></NavLink>
            <NavLink className = "navigation" to = "/news"><p className = "menuItem">News</p></NavLink>
            {/* <NavLink className = "navigation" to = "/plan"><p className = "menuitem">Plans</p></NavLink> */}
        </nav>
    )
}

export { NavBar }
