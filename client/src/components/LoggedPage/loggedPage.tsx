import { NavBar } from './NavBar.component'
import  Header  from './Header.component'
import NewProfile from '../ProfilePage/NewProfile.component'
import { BrowserRouter, Route } from 'react-router-dom';
import MyfriendsPage from '../MyFriendsPage/Myfriends.component'
import  NewsPage from '../NewsPage/NewsPage'
import './loggedPage.styles.css'
import FindNewFriend from '../FindNewFriendsPage/FindNewFriend.component'
import MessagesPage from '../MessagesPage/MessagesPage';
import Chat from '../MessagesPage/Chat.component'
import Armory from '../CheckOut/ArmoryContainer';
// import Plans from '../Plans';

const loggedPage = () => {
    return (
        <div className = "page-wrapper">
            <BrowserRouter>
                <Header/>
                <div className = "second-row">
                    <NavBar/>
                    <div className = "content">
                        <Route exact path = '/payment' component = {Armory}/>
                        <Route exact path = '/profile' component = {NewProfile}/>
                        <Route exact path = '/myfriends' component = {MyfriendsPage}/>
                        <Route exact path = '/findfriends' component = {FindNewFriend}/>
                        <Route exact path = '/messages' component = {MessagesPage}/>
                        <Route exact path = '/news' component = {NewsPage}/>
                        <Route path = '/chat' component = {Chat}/>
                        {/* <Route path = '/plans' component = {Plans}/> */}
                    </div>
                </div>
            </BrowserRouter>
        </div>
    )
}

export default loggedPage


