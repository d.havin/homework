import React from "react";
import { shallow } from "enzyme";
import  Header  from "./Header.component";

const setUp = (props = {}) => {
    const component = shallow(<Header {...props}/>);
    return component;
};

describe('Header Component', () => {
    let component;
    beforeEach(() => {
        component = setUp();
    });

    it('It should render without errors', () => {
        const wrapper = component.find('.header-block'); //возможно объявление через data-test: ...find(`[data-test = 'header-block']`)
        expext(wrapper.length).toBe(1);
    });

    it('Should render a logo', () => {
        const logo = component.find('.header-img');
        expext(logo.length).toBe(1);
    });

    it('Should render a title', () => {
        const title = component.find('.header-title');
        expext(title.length).toBe(1);
    });
}) 