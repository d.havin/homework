import UserDataComponent from "./UserData.component";
import { store } from "../../store/store";
import { Provider } from "react-redux";

const StoredUserData = () => {
    return (
      <Provider store = {store}>
        <UserDataComponent/>
      </Provider> 
    )
}

export default StoredUserData