import { useState } from 'react';
import { connect } from "react-redux";
import { fetchAddPost } from '../../actionCreator/user.action'
import './addPost.css'

const AddPost = (props: any) => {
    const [ postDescription, setpostDescription ] = useState('')
    const [ postMedia, setPostMedia ] = useState('')
    let [ postInputVisibility, setPosttarInputVisibility ] = useState(false)
    let [ valueAddPost, setvalueAddPost ] = useState('Add media')

    const handleChangeVision = () => {
        postInputVisibility === false ? setPosttarInputVisibility(postInputVisibility = true) : setPosttarInputVisibility(postInputVisibility = false)
        valueAddPost === 'Add media' ? setvalueAddPost(valueAddPost = "cancel") : setvalueAddPost(valueAddPost = 'Add media')
    }

    const handleAddPost = () => {
        let post = {
            owner: props.user._id,
            description: postDescription,
            media: postMedia,
        }
        props.fetchAddPost(post);
        setPosttarInputVisibility(postInputVisibility = false)
        setPostMedia('')
        setpostDescription('')
    }

    return (
        <div className = "addPostBlock">
            <p>Create post: </p>
            <div className = "Input_post_description">
                <img src={props.user.avatar} className = "Post_avatar" alt ="img"/>
                <textarea value={postDescription} className = "Post_description" placeholder = "Whats new?" onChange={(e) => setpostDescription(e.target.value)}/>
            </div>
            <div className = "publicate_block">
                <input className = "change_ava_button" type = "submit" onClick = {handleChangeVision} value = {valueAddPost}/>
                {postInputVisibility === false ? null :
                    <input className = "change_ava_input" value={postMedia} placeholder = "Set media URL" onChange={(e) => setPostMedia(e.target.value)}/>
                }
                <input className="change_ava_button" type = "submit" onClick={handleAddPost} style = {{backgroundColor: "rgba(124, 226, 124, 0.84)", color: "white"}} value = "Publicate"/>
            </div>
        </div>
    )
}

const mapStateToProps = (state: any) => ({
    user: state.auth.logedUser
});

const mapDispatchToProps = (dispatch: any) => ({
    fetchAddPost: (data: any) => {dispatch(fetchAddPost(data))}
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddPost);