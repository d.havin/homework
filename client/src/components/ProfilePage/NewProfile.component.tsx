import StoredUserData  from './UserData.component'
import AddPost  from './AddPost.component'
import MyPosts  from './MyPosts.component'

function NewProfile(props:any) {

  return (
    <>
        <StoredUserData/>
        <AddPost/>
        <MyPosts/>
    </> 
  );
}

export default NewProfile;
