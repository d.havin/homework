import React from "react";
import { create, act } from "react-test-renderer";
import StoredUserdata from "./StoredUserdata";

describe("UserData component render", () => {

    test('Avatar image from props should be in the state', () => {
        let component;
        act(() => {
            component = create(<StoredUserdata avaImg="avatar image from props"/>)
        })

        const instance = component.getInstance();
        // const instance = component.root
        // console.log(instance)
        
        // expect(instance.state.avaImg).toBe("avatar image from props")
    })
})

// import React, { useState } from "react";
// import ReactDOM from "react-dom";
// import { act } from "react-dom/test-utils";

// let container;

// beforeEach(() => {
//   container = document.createElement("div");
//   document.body.appendChild(container);
// });

// afterEach(() => {
//   document.body.removeChild(container);
//   container = null;
// });

// function Button(props) {
//   const [text, setText] = useState("");
//   function handleClick() {
//     setText("PROCEED TO CHECKOUT");
//   }
//   return <button onClick={handleClick}>{text || props.text}</button>;
// }

// describe("Button component", () => {
//   test("it shows the expected text when clicked", () => {
//     act(() => {
//       ReactDOM.render(<Button text="SUBSCRIBE TO BASIC" />, container);
//     });
//     const button = container.getElementsByTagName("button")[0];
//     expect(button.textContent).toBe("SUBSCRIBE TO BASIC");
//     act(() => {
//       button.dispatchEvent(new MouseEvent("click", { bubbles: true }));
//     });
//     expect(button.textContent).toBe("PROCEED TO CHECKOUT");
//   });
// });

// Вот мои предпочтительные инструменты для тестирования приложений React:

// React-Test-Renderer для модульного тестирования моментальных снимков
// Act API для модульного тестирования компонентов React
// Jest для модульного и интеграционного тестирования кода JavaScript
// Cypress для сквозного / UI тестирования
// Я также предлагаю взглянуть на react-testing-library , красивую оболочку для API Act.