import StripeContainer from "./StripeContainer"
import Car from "../../assets/Car.jpg"
import { useState } from "react"
import "./checkOutPage.css"
import ArtPayForm from "./artPayComponent"

export default function Armory(){
    const [showItem, setShowItem] = useState(false)

    return (
        <div className = "armoryPage">
            {showItem ? <StripeContainer/> : <><h1>Evil machine</h1><h3>$10.00</h3><img className = "armor-image" src = {Car} alt = "Car"/>
            <button onClick = {() => setShowItem(true)}>Purchase</button></>}
            <ArtPayForm/>
        </div>
    )
}