import { Elements } from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'
import CheckoutForm from './ChekcoutForm'

const PUBLIC_KEY = "pk_test_51Jh8JGB0VwhYqY0WrLMKMeJlvx0ffgkM6gTd9VRU15xBnL5nSkjx52meQXaQfvvtF0Giqm12zF2n3vRGSQqCa1Ye00qSG6ioUF"
const stripePromise = loadStripe(PUBLIC_KEY)

export default function StripeContainer() {
    return (
        <Elements stripe = {stripePromise}>
            <CheckoutForm/>
        </Elements>
    )
}