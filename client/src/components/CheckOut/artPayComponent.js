import { useState } from "react";
// import axios from "axios";

export default function ArtPayForm(){
    const [apStoriedValue, setApStoriedValue] = useState("57003");
    const [apOrderNum, setApOrderNum] = useState('726175');
    const [apAmount, setApAmount] = useState('1000');
    const [apCurrency, setApCurrency] = useState('BYN');
    const [apClientDt, setApClientDt] = useState('1466602065');
    const [description, setDescription] = useState('Evil Machine');
    const [apSignature, setApSignature] = useState('8583ea5313741aa02dd668c1b9a5cfcb3dd860510b53ac921af8d3a92093b435a4e08a22b10fdbe3b99c94b3372edabe44aaafc41db4a68928fc425edc59c5af')

    const json = {
        ap_storied: apStoriedValue,
        ap_order_num: apOrderNum,
        ap_amount: apAmount,
        ap_currency: apCurrency,
        ap_client_dt: apClientDt,
        ap_invoice_desc: description,
        ap_test: "true",
        ap_signature: apSignature
    }

    const url = `ap_storied=${apStoriedValue}&ap_order_num=${apOrderNum}&ap_amount=${apAmount}&ap_currency=${apCurrency}&ap_client_dt=${apClientDt}&ap_invoice_desc=${description}&ap_test=1&ap_signature=${apSignature}`

    const handleSubmit = async (event) => {  
        event.preventDefault();
        console.log(url)
        // let res = JSON.stringify(json)
        // console.log(res)
        // const paymentRequestUrl = window.location.pathname.substr(23)
        const result = await fetch(`https://gateway-sandbox-artpay.dev-3c.by/create/`, { // ${url}  надо ли подставлять это в запрос??
            method: "POST",
            // mode: "no-cors",                 //отключение в текущем запросе
            // credentials: "include",          //памятка на исп куков
            headers: {
                "Content-type": "application/json; charset=UTF-8" 
                },
            body: JSON.stringify(json), 
        })
        .then(res => res.json())
        console.log(result)
    };

//incorrect data in ap_signature  <=== ошибка при выполнении запроса

    //"application/json"
    // application/x-www-form-urlencoded
    // multipart/form-data
    // text/plain

    return (
        <div className = "artPayForm">
            <form onSubmit = { handleSubmit } id = "payment-form">
                <input type = "hidden" name="ap_storeid" value = {apStoriedValue}/>
                <input type = "hidden" name="ap_order_num" value = {apOrderNum}/>
                <input type = "hidden" name="ap_amount" value = {apAmount}/>
                <input type = "hidden" name="ap_currency" value = {apCurrency}/>
                <input type = "hidden" name="ap_client_dt" value = {apClientDt}/>
                <input type = "hidden" name="ap_invoice_desc" value = {description}/>
                <input type = "hidden" name="ap_test" value = "true"/>
                <input type = "hidden" name="ap_signature" value = {apSignature}/>
                <input type = "submit" value="Оплатить"/>
            </form>
        </div>
    )
}
