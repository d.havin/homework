import { useState } from "react";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import "./checkOutPage.css"
import axios from "axios";
import moon from "../../assets/moon.jpg";
import minion from "../../assets/minion.jpeg";

export default function CheckoutForm() {
  const [success, setSucceeded] = useState(false);

  const [error, setError] = useState(null);
  const [processing, setProcessing] = useState('');
  const [disabled, setDisabled] = useState(true);
 
  const stripe = useStripe()
  const elements = useElements()

  const cardStyle = {
    style: {
      base: {
        color: "#32325d",
        fontFamily: 'Arial, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#32325d"
        }
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    }
  };

  const handleChange = async (event) => {           //проверка данных карты при вводе
    setDisabled(event.empty);
    setError(event.error ? event.error.message : "");
  };

  const handleSubmit = async (ev) => {
    ev.preventDefault();
    setProcessing(true);
    const {error, paymentMethod} = await stripe.createPaymentMethod({
        type: "card",
        card: elements.getElement(CardElement)
    })

    if (!error) {
      try {
        setError(null);
        setProcessing(false);
        const {id} = paymentMethod
        const response = await axios.post("http://localhost:3050/payment", {
          amount: 1000,
          id
        })
        if (response.data.success) {
          setSucceeded(true);
        }
        if (response.data.error) {
          setError(response.data.error.raw.message)
        }
      } catch (e) {
        setError(`Payment failed ${e.message}`);
        setProcessing(false);
      }
    } else {
      setError(`Payment failed ${error.message}`);
      setProcessing(false);
    }
  };

  return (
    <>
      {!success ?
      <form onSubmit = { handleSubmit } id = "payment-form">
        <CardElement id = "card-element" options = { cardStyle } onChange = { handleChange }/>
        <button disabled = { processing || disabled || success } id = "submit">
          <span id = "button-text">
            {processing ? (<div className = "spinner" id = "spinner"></div>): ("Pay now")}
          </span>  
        </button>
        
        {/* ошибки с картой, возникающие при обработке платежа */}
        { error && (
          <div className = "card-error" role = "alert">
            {error}
          </div>
        )}
      </form>
      :
      <div className = { success ? "result-message" : "result-message hidden"}>
        <p>You just bought a car to do great evil. It's time to start!!!</p>
        <p>{"..."}</p>
        <p className = "qwest">Want you steal the moon may be?</p>
        <img className = "moon-image" src = { moon } alt = "moon"/>
        <img className = "minion" src = { minion } alt = "minion"/>
      </div>
      }
    </>
  )
}
