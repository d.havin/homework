import { connect } from "react-redux";
import { Route, Redirect } from 'react-router-dom';

function PrivateRoute({ component: Component, auth, ...rest }) {

    return <Route
        { ...rest }
        render = { props => {
            return (
                auth.isAuth ? (
                    <Redirect to ="/profile"/>
                ) : <Component { ...props }/>
            )
        }}
    />
}

const mapStateToProps = (state) => ({
    auth: state.auth
});

export default connect(
    mapStateToProps
)(PrivateRoute);