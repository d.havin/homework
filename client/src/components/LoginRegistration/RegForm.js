import { Formik, Form } from "formik"
import { TextField } from "./TextField";
import * as Yup from "yup";
import { fetchRegistrationNewUser } from '../../actionCreator/user.action';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';

const RegForm = (props) => {
    const validationOptions = Yup.object({
        name: Yup.string()
            .min(2, "Must be 2 characters or more")
            .max(15, "Must be 15 chracters or less")
            .required("Required"),
        surname: Yup.string()
            .min(2, "Must be 2 characters or more")
            .max(15, "Must be 15 chracters or less")
            .required("Required"),
        email: Yup.string()
            .email("Email is invalid")
            .required("Email is required"),
        password: Yup.string()
            .min(6, "Password must be at least 6 characters")
            .max(40, "Must be 40 chracters or less")
            .required("Password is required"),
        confirmPassword: Yup.string()
            .oneOf([Yup.ref('password'), null], "Password must match")
            .required("Confirm password is required")
    })

    return (
        <Formik
            initialValues = {{
                name: '',
                surname: '',
                email: '',
                password: '',
                confirmPassword: ''
            }}
            validationSchema = {validationOptions}
            onSubmit = {user => {
                props.fetchRegistrationNewUser(user)
            }}
        > 
            <div className="RegistrationBlock">
                <h1 className="sign-title">Registration</h1>
                <Form className="reg-form">
                    <TextField label="Name" name="name" type="text" placeholder = "Eva"/>
                    <TextField label="Surname" name="surname" type="text" placeholder = "Poleschuk"/>
                    <TextField label="Email" name="email" type="text" placeholder = "Eva@mail.ru"/>
                    <TextField label="Password" name="password" type="text" placeholder = "*******"/>
                    <TextField label="Confirm password" name="confirmPassword" type="text" placeholder = "*******" />
                    <div>
                        <button type="submit" className="Button_Registration">Register</button>
                        <button type="reset" className="Button_Reset">Reset</button>
                    </div>
                    <div className="already">
                        Already have an account?<Link to="/"><p className="link-reg">Sign in</p></Link>
                    </div>
                    <div className="Status_block" >
                        {props.auth.loading ? <p>Loading...</p> : props.auth.error ? <p>Error... Try again</p> : props.auth.message}
                    </div>
                </Form>
            </div>
        </Formik>
    )
}

const mapStateToProps = (state) => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => ({
    fetchRegistrationNewUser: (data) => dispatch(fetchRegistrationNewUser(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegForm);