import { useState } from 'react';
import { connect } from "react-redux";
import { Link } from "react-router-dom"
import { fetchLoginUser } from '../../actionCreator/user.action'
import './auth.styles.css'

const AuthForm = (props: any) => {
    const [inputedEmail, setEmail] = useState("");
    const [inputedPassword, setPassword] = useState("");

    const handleLog = () => {
        let user = {
            email: inputedEmail,
            password: inputedPassword 
        };
        props.fetchLoginUser(user);  
    }

    return ( 
        <div className = "RegistrationBlock">
            <div className = "Title_Registration">Welcome</div>
            <input className = "Input_Registration" type = "text" placeholder ="Email" value={inputedEmail} onChange={(e) => setEmail(e.target.value)}/>
            <input className = "Input_Registration" type = "password" placeholder ="Password" value={inputedPassword} onChange={(e) => setPassword(e.target.value)}/>
            <br/>
            <input className="Button_Registration" type = "submit" value = "Log in" onClick={handleLog}/>

            <div className = "already">
                don't have an account?<Link to = "/registration"><p className = "link-reg">Registration</p></Link>
            </div>
            <img className = "botttom-image-log" src = "https://i.pinimg.com/originals/46/22/a9/4622a9b6ce0a8fbdff3c880d1da18e88.jpg" alt = "img"></img>
            <div className = "Status_block" >
                {props.auth.loading? <p>Loading...</p> : props.auth.error ? <p>Error... Try again</p> : null}
            </div>
        </div>
    )
}

const mapStateToProps = (state: any) => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch: any) => ({
    fetchLoginUser: (data: any) => dispatch(fetchLoginUser(data)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthForm);