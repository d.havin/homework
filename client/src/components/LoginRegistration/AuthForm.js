import { Formik, Form } from "formik"
import { TextField } from "./TextField";
import * as Yup from "yup";
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import { fetchLoginUser } from "../../actionCreator/user.action";
import './auth.styles.css'

const AuthForm = (props) => {
    const validationOptions = Yup.object({
        email: Yup.string()
            .email("Email is invalid")
            .required("Email is required"),
        password: Yup.string()
            .min(6, "Password must be at least 6 characters")
            .max(40, "Must be 40 chracters or less")
            .required("Password is required")
    })

    return (
        <Formik
            initialValues={{
                email: "",
                password: ""
            }}
            validationSchema={validationOptions}
            onSubmit={user => {
                props.fetchLoginUser(user)
            }}
        >
            <div className="RegistrationBlock">
                <h1 className="sign-title">SignUp</h1>
                <Form className="reg-form">
                    <TextField label="Email" name="email" type="text" />
                    <TextField label="Password" name="password" type="text" />
                    <button type="submit" className="Button_Registration">LogIn</button>
                    <div className="already">
                        Don't have an account?<Link to="/registration"><p className="link-reg">Registration</p></Link>
                    </div>
                    <img className="botttom-image-log" src="https://i.pinimg.com/originals/46/22/a9/4622a9b6ce0a8fbdff3c880d1da18e88.jpg" alt="img"></img>
                    <div className="Status_block" >
                        {props.auth.loading ? <p>Loading...</p> : props.auth.error ? <p>Error... Try again</p> : null}
                    </div>
                </Form>
            </div>
        </Formik>
    )
}

const mapStateToProps = (state) => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => ({
    fetchLoginUser: (data) => dispatch(fetchLoginUser(data)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthForm);