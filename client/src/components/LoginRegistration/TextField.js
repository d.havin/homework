import React, { useState } from "react"
import { ErrorMessage, useField } from "formik";
import "./auth.styles.css"

export const TextField = ({ label, ...props}) => {
    const [field, meta] = useField(props);
    // const [values, setValues] = useState();
    // console.log(field)
    // console.log(meta)

    return (
        <div className = "input-wrapper">
            <label className = "input-label" htmlFor = {field.name}>{label}</label>
            <input className = {`Input_Registration ${meta.touched && meta.error && 'is-invalid'}`} 
            {...field} 
            {...props} 
            // value = {values} 
            // onChange = {(e) => {setValues(e.target.value)}}
            // onBlur = {field.onBlur}
            />
            <ErrorMessage component = "div" className = "input-error" name = {field.name}/>
        </div>
    )
}