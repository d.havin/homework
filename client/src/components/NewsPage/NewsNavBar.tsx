import './newsNavbar.styles.css'

const NewsNavBar = (props: any) => {
    return (
        <nav className = "newsnavbarblock">
            <div className = "news-navigation" onClick = {() => props.handleFunc("news")}><p className = "menuItem">Last news</p></div>
            <div className = "news-navigation" onClick = {() => props.handleFunc("myfriendsposts")}><p className = "menuItem">My friends posts</p></div>
            <div className = "news-navigation" onClick = {() => props.handleFunc("myposts")}><p className = "menuItem">My posts</p></div>
        </nav>
    )
}

export { NewsNavBar }