// import AuthForm from './components/LoginRegistration/Auth.component';
import RegistrationForm from './components/LoginRegistration/Registration.component';
import { BrowserRouter } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute.component';
import PublicRoute from './components/PublicRoute';
import loggedPage from './components/LoggedPage/loggedPage';
import { Provider } from 'react-redux';
import { store } from "./store/store"

import RegForm from "./components/LoginRegistration/RegForm"
import AuthForm from "./components/LoginRegistration/AuthForm"

// function App() {
//   return (
//     <BrowserRouter>
//       <PublicRoute exact path = '/' component = {AuthForm}/>
//       <PublicRoute exact path = '/registration' component = {RegistrationForm}/>
//       <PrivateRoute exact path = '/profile' component = {loggedPage}/>
//     </BrowserRouter>
//   );
// }

const AppGeneral = () => {
  return (
    <Provider store = {store}>
      <BrowserRouter>
          <PublicRoute exact path = '/' component = {AuthForm}/>
          <PublicRoute exact path = '/registration' component = {RegForm}/>
          <PrivateRoute exact path = '/profile' component = {loggedPage}/>
      </BrowserRouter>
    </Provider> 
  )
}

export default AppGeneral;
