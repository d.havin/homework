import {
  userRegistrationRequest,
  userRegistrationRequestSuccess,
  userRegistrationRequestFailed,
  userLoginRequest,
  userLoginRequestSuccess,
  userLoginRequestFailed,
  changeAvatarRequest,
  changeAvatarRequestSuccess,
  changeAvatarRequestFailed,
  getAllPostsRequest,
  getAllPostsRequestSuccess,
  getAllPostsRequestFailed,
  getAllMyFriendsPostRequest,
  getAllMyFriendsPostRequestSuccess,
  getAllMyFriendsPostRequestFailed,
  getAllMyPostsRequest,
  getAllMyPostsRequestSuccess,
  getAllMyPostsRequestFailed,
  addPostRequest,
  addPostRequestSuccess,
  addPostRequestFailed,
  deletePostRequest,
  deletePostRequestSuccess,
  deletePostRequestFailed,
  getAllUsersRequestSuccess,
  getAllUsersRequestFailed,
  addFriendRequestSuccess,
  cancelRequestSuccess,
  applyInvitation,
  rejectInvitationSuccess,
  deleteFriendSuccess,
  getAllMyInvitationsSuccess,
  getAllMyRequestsSuccess,
  getAllMyFriendsSuccess,
  likePostSuccess,

  createChatSuccsess,
  // allChatsSuccess,
  getChatSuccess,

} from "../actionCreator/user.action";

import { put, call, takeEvery } from "redux-saga/effects";

export function* watchFetchUser() {
    yield takeEvery("FETCH_REGISTRATE_USER", fetchRegistrationNewUserAsync )
    yield takeEvery("FETCH_LOGIN_USER", fetchLoginUserAsync)
    yield takeEvery("FETCH_CHANGE_AVATAR", fetchChangeAvatarAsync)

    yield takeEvery("FETCH_GET_ALL_POSTS", fetchGetAllPostsAsync)
    yield takeEvery("FETCH_GET_ALL_MY_FRIENDS_POSTS", fetchGetAllMyFriendsPostAsync)
    yield takeEvery("FETCH_GET_ALL_MY_POSTS", fetchGetAllMyPostsAsync)
    yield takeEvery("FETCH_ADD_POST", fetchAddPostAsync)
    yield takeEvery("FETCH_DELETE_POST", fetchDeletePostAsync)
    yield takeEvery("FETCH_LIKE_POST", fetchLikePostAsync)

    yield takeEvery("FETCH_GET_ALL_USERS", fetchGetAllUsersAsync)

    yield takeEvery("FETCH_DELETE_FRIEND", fetchDeleteFriendAsync)
    yield takeEvery("FETCH_ADD_FRIEND", fetchAddFriendAsync)
    yield takeEvery("FETCH_CANCEL_REQUEST", fetchCancelRequestAsync)
    yield takeEvery("FETCH_APPLY_INVITATION", fetchApplyInvitationAsync)
    yield takeEvery("FETCH_REJECT_INVITATION", fetcjRejectInvitationAsync)

    yield takeEvery("FETCH_GET_ALL_MY_INVITATIONS", fetchGetAllMyInvitationAsync)
    yield takeEvery("FETCH_GET_ALL_MY_REQUESTS_SUCCESS", fetchgetAllMyRequestsAsync)
    yield takeEvery("FETCH_GET_ALL_MY_FRIENDS", fetchgetAllMyFriendsAsync)
  
    yield takeEvery("FETCH_CREATE_CHAT", fetchCreateChatSuccessAsync)
    yield takeEvery("FETCH_GET_CHAT", fetchGetChatAsync)
}
//=======================================================================AUTH
function* fetchRegistrationNewUserAsync(action) {
    try {
      yield put(userRegistrationRequest());
      yield call(() => {                         
        return fetch("http://localhost:3050/auth/registration", {
          method: "POST",
          body: JSON.stringify(action.data),
          headers: {
            "Content-type": "application/json; charset = UTF-8",
          },
        })
      });
      yield put(userRegistrationRequestSuccess(action.data));
    } catch (err) {
      console.log(err)
      yield put(userRegistrationRequestFailed());
    }
}
function* fetchLoginUserAsync(action) {
  try {
    yield put(userLoginRequest());
    const data = yield call(() => {                         
      return fetch("http://localhost:3050/auth/login", {
        method: "POST",
        body: JSON.stringify(action.data),
        headers: {
          "Content-type": "application/json; charset = UTF-8",
        },
      }).then(res => res.json())
    });
    localStorage.setItem("token", data.token)
    yield put(userLoginRequestSuccess(data));
  } catch (error) {
    console.log(error)
    yield put(userLoginRequestFailed());
  }
}
function* fetchChangeAvatarAsync(action) {
  try {
    yield put(changeAvatarRequest());

    const data = yield call(() => {                         
      return fetch("http://localhost:3050/profile/changeavatar", {
        method: "POST",
        body: JSON.stringify(action.data),
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
      }).then(res => res.json())
    });
      
    yield put(changeAvatarRequestSuccess(data));
  } catch (error) {
    console.log(error)
    yield put(changeAvatarRequestFailed());
  }
}
//========================================================================POST
function* fetchAddPostAsync(action) {
  try {
    yield put(addPostRequest());
    const data = yield call(() => {                         
      return fetch("http://localhost:3050/posts/create", {
        method: "POST",
        body: JSON.stringify(action.data),
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
      }).then(res => res.json())
    });
    yield put(addPostRequestSuccess(data.posts));
  } catch (err) {
    console.log(err)
    yield put(addPostRequestFailed());
  }
}
function* fetchDeletePostAsync(action) { 
  try {
    yield put(deletePostRequest());
    const data = yield call(() => {                         
      return fetch("http://localhost:3050/posts/delete", {
        method: "DELETE",
        body: JSON.stringify(action.data),
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
      }).then(res => res.json())
    });
    yield put(deletePostRequestSuccess(data.posts));
  } catch (err) {
    console.log(err)
    yield put(deletePostRequestFailed());
  }
}
function* fetchLikePostAsync(action) { 
  try {  
    console.log(action)
    const data = yield call(() => {                         
      return fetch("http://localhost:3050/posts/like", {
        method: "PUT",
        body: JSON.stringify(action.data),
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
      }).then(res => res.json())
    });
    yield put(likePostSuccess(data.posts, data.myFriendsPosts, data.myPosts));
  } catch (err) {
    console.log(err)
  }
}
//====================================================================Get Posts
function* fetchGetAllPostsAsync(action) {           
  try {
    yield put(getAllPostsRequest());
    const data = yield call(() => {                         
      return fetch("http://localhost:3050/posts/allposts ", {
        method: "GET",
        body: JSON.stringify(action.data),
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
      }).then(res => res.json())
    });
    yield put(getAllPostsRequestSuccess(data.posts));
  } catch (err) {
    console.log(err)
    yield put(getAllPostsRequestFailed());
  }
}
function* fetchGetAllMyFriendsPostAsync(action) {   
  try {
    yield put(getAllMyFriendsPostRequest());
      const data = yield call(() => {                         
      return fetch("http://localhost:3050/posts/myfriendsposts", {
        method: "GET",
        body: JSON.stringify(action.data),
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
      }).then(res => res.json())
    });
    yield put(getAllMyFriendsPostRequestSuccess(data.myFriendsPosts));
    
  } catch (err) {
    console.log(err)
    yield put(getAllMyFriendsPostRequestFailed());
  }
}
function* fetchGetAllMyPostsAsync(action) {         
  try {
    yield put(getAllMyPostsRequest());
      const data = yield call(() => {                         
      return fetch("http://localhost:3050/posts/myposts", {
        method: "GET",
        body: JSON.stringify(action.data),
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
      }).then(res => res.json())
    });
    yield put(getAllMyPostsRequestSuccess(data.posts));
  } catch (err) {
    console.log(err)
    yield put(getAllMyPostsRequestFailed());
  }
}
//===================================================================Get All Users
function* fetchGetAllUsersAsync() {
  try {
      const data = yield call(() => {                         
      return fetch("http://localhost:3050/findnewfriends/allUsers", {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
      }).then(res => res.json())
    });
    yield put(getAllUsersRequestSuccess(data.users));
  } catch (err) {
    console.log(err)
    yield put(getAllUsersRequestFailed());
  }
}
//======================================================================CRUD FRIENDS
function* fetchAddFriendAsync(action) {                                         
  try {
      const data = yield call(() => {                         
      return fetch("http://localhost:3050/findnewfriends/addFriend", {
        method: "PUT",
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
        body: JSON.stringify(action.data),
      }).then(res => res.json())
    });
    yield put(addFriendRequestSuccess(data.users, data.friendsShipRequests));
  } catch (err) {
    console.log(err)
  }
} 
function* fetchCancelRequestAsync(action) {                       
  try {
      const data = yield call(() => {                         
      return fetch("http://localhost:3050/findnewfriends/cancelRequest", {
        method: "PUT",
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
        body: JSON.stringify(action.data),
      }).then(res => res.json())
    });
    yield put(cancelRequestSuccess(data.users, data.friendsShipRequests));
  } catch (err) {
    console.log(err)
  }
} 
function* fetchDeleteFriendAsync(action) {                         
  try {
    console.log(action)
      const data = yield call(() => {                         
      return fetch("http://localhost:3050/friends/deleteFriend", {
        method: "DELETE",
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
        body: JSON.stringify(action.data),
      }).then(res => res.json())
    });
    yield put(deleteFriendSuccess(data.users, data.friendsList));
  } catch (err) {
    console.log(err)
  }
} 
function* fetchApplyInvitationAsync(action) {                            
  try {
    console.log(action)
      const data = yield call(() => {                         
      return fetch("http://localhost:3050/findnewfriends/apply", {
        method: "POST",
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
        body: JSON.stringify(action.data),
      }).then(res => res.json())
    });
    yield put(applyInvitation(data.users, data.invitations, data.friends));
  } catch (err) {
    console.log(err)
  }
} 
function* fetcjRejectInvitationAsync(action) {                               
  try {
      const data = yield call(() => {                         
      return fetch("http://localhost:3050/findnewfriends/rejectInvitation", {
        method: "PUT",
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
        body: JSON.stringify(action.data),
      }).then(res => res.json())
    });
    yield put(rejectInvitationSuccess(data.users, data.invitations));
  } catch (err) {
    console.log(err)
  }
} 
//============================================================================ Friends Page
function* fetchGetAllMyInvitationAsync() {
  try {
      const data = yield call(() => {                         
      return fetch("http://localhost:3050/friends/invitations", {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
      }).then(res => res.json())
    });
    yield put(getAllMyInvitationsSuccess(data.users));
  } catch (err) {
    console.log(err)
  }
}
function* fetchgetAllMyRequestsAsync() {
  try {
      const data = yield call(() => {                         
      return fetch("http://localhost:3050/friends/requests", {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
      }).then(res => res.json())
    });
    yield put(getAllMyRequestsSuccess(data.users));
  } catch (err) {
    console.log(err)
  }
}
function* fetchgetAllMyFriendsAsync() {
  try {
      const data = yield call(() => {                         
      return fetch("http://localhost:3050/friends/myfriends", {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
      }).then(res => res.json())
    });
    yield put(getAllMyFriendsSuccess(data.users));
  } catch (err) {
    console.log(err)
  }
}
//============================================================================= CHAT
function* fetchCreateChatSuccessAsync(action) {                         
  try {
      const data = yield call(() => {                         
      return fetch("http://localhost:3050/chats/createchat", {
        method: "POST",
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
        body: JSON.stringify(action.data),
      }).then(res => res.json())
    });
    yield put(createChatSuccsess(data.chat));
  } catch (err) {
    console.log(err)
  }
} 

function* fetchGetChatAsync(action) {           
  try {
    const data = yield call(() => {                         
      return fetch("http://localhost:3050/chats/getchat ", {
        method: "POST",
        body: JSON.stringify(action.data),
        headers: {
          "Content-type": "application/json; charset = UTF-8",
          'authorization' : `${localStorage.getItem('token')}`
        },
      }).then(res => res.json())
    });
    yield put(getChatSuccess(data.chat));
  } catch (err) {
    console.log(err)
  }
}








 