import {createStore, combineReducers, applyMiddleware} from "redux";
import createSagaMiddleware from 'redux-saga'
import {watchFetchUser} from '../saga/saga'
import authReducer from '../reducers/auth.reducer'
import friendReducer from '../reducers/friends.reducer'
import postReducer from '../reducers/posts/post.reducer'
import chatReducer from '../reducers/chat.reducer'


const sagaMiddleware = createSagaMiddleware();

export const rootReducer = combineReducers({
    auth: authReducer,
    users: friendReducer,
    posts: postReducer,
    chat: chatReducer
});

export const store = createStore( rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(watchFetchUser);  