import mongoose from "mongoose";
import autopopulate from "mongoose-autopopulate";
import { IUser } from "./user.model";
import { IMessage } from "./message.model";
const { Schema } = mongoose;

interface IChat extends mongoose.Document {
    chatParticipants: [mongoose.Schema.Types.ObjectId];
    messages: Array<IMessage>;
    date: Date;
}

const chatSchema = new Schema(
  {
    chatParticipants: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      require: true,
      autopopulate: {maxDepth : 2}
    }],
    messages: [{ type: mongoose.Schema.Types.ObjectId, ref: "Message", autopopulate: {maxDepth : 2}}],
    date: { type: Date, default: Date.now() },
  },
  { versionKey: false }
);

chatSchema.plugin(autopopulate);
const Chat = mongoose.model<IChat>("Chat", chatSchema);

export { Chat, IChat, chatSchema };
