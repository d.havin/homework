import mongoose from "mongoose";
import autopopulate from "mongoose-autopopulate";
import { IUser } from "./user.model";
import { IComment } from "./comment.model";
const { Schema } = mongoose;

interface IPost extends mongoose.Document {
  owner: IUser;
  description: String;
  media: String;
  likes: Array<IUser>;
  comments: Array<IComment>;
  date: Date;
}

const postSchema = new Schema(
  {
  owner: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: "User", 
    autopopulate: { maxDepth: 3 } }, 
  description: { type: String, required: true },
  media: { type: String, default: null },                                      
  likes: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  }],                                                             
  comments: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Comment",
    autopopulate: { maxDepth: 2 },
  },
  date: { type: Date, default: Date.now() },
  },
  { versionKey: false }
);

postSchema.plugin(autopopulate);
const Post = mongoose.model<IPost>("Post", postSchema);

export { Post, IPost, postSchema };
