import mongoose from "mongoose";
import autopopulate from "mongoose-autopopulate";
const { Schema } = mongoose;
import { IChat } from "./chat.model";
import { IPost } from "./post.model";

interface IUser extends mongoose.Document {
  name: string;
  surname: string;
  email: string;
  password: string;
  avatar: string;   
  friends: Array<IUser>;
  chats: [mongoose.Schema.Types.ObjectId];
  posts: Array<IPost>;
  invitations: Array<IUser>;
  friendshipRequests: Array<IUser>;
}
//дубляж
const userSchema = new Schema(
  {
    name: { type: String },
    surname: { type: String },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    avatar: {
      type: String,
      default:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAurS13TnP6GUZntGWYSu7qZFYO0dUB8rnBg&usqp=CAU",
    },
    friends: [
      { type: mongoose.Schema.Types.ObjectId, ref: "User", autopopulate: { maxDepth: 2 } },
    ],
    chats: [
      { type: mongoose.Schema.Types.ObjectId, ref: "Chat", autopopulate: { maxDepth: 3 } },
    ],
    posts: [
      { type: mongoose.Schema.Types.ObjectId, ref: "Post", autopopulate: { maxDepth: 2 } },
    ],
    invitations: [
      { type: mongoose.Schema.Types.ObjectId, ref: "User", autopopulate: { maxDepth: 2 } },
    ],
    friendshipRequests: [
      { type: mongoose.Schema.Types.ObjectId, ref: "User", autopopulate: { maxDepth: 2 } },
    ],
  },
  { versionKey: false }
);

userSchema.plugin(autopopulate);
const User = mongoose.model<IUser>("User", userSchema);

export { User, IUser, userSchema };
