import mongoose from "mongoose";

interface IUser extends mongoose.Document {
  name: String;
  surname: String;
  email: String;
  password: String;
  avatar: String;
  friends: Array<IUser>;
  chats: Array<IChat>;
  posts: Array<IPost>;
}

interface IPost extends mongoose.Document {
  owner: IUser;
  title: String;
  description: String;
  picture: String; // ????????
  video: String;
  likes: Array<IUser>;
  comments: Array<IComment>;
  date: Date;
}

interface IComment extends mongoose.Document {
  commentedPost: IPost;
  commentOwner: IUser;
  commentOwnerAva: String; // ???????
  commentText: String;
  date: Date;
}

interface IChat extends mongoose.Document {
  _id: String;
  chatParticipant1: IUser;
  chatParticipant2: IUser; // ???????
  messages: IMessage;
  date: Date;
}

interface IMessage extends mongoose.Document {
  text: String;
  chat: IChat;
  messageSender: IUser;
  date: Date;
}
