import { User, IUser } from '../db/db.models/user'
import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken';
import { validationResult } from "express-validator";
import bcrypt from 'bcryptjs'

interface CustomRequest<T>extends Request {
    body: T
} 

const registrationSql = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        const errors: any = validationResult(request)
        if (!errors.isEmpty()) {
            let err = errors;
            return response.status(400).send(` error message: ${err.errors[0].msg}`)
        }
        const { name, surname, email, password} = request.body
        // let user = await User.findOne({ email });
        // if (user) {
        //     return response.status(400).json({message: "This email is already in use"})
        // }
        const hashPassword = bcrypt.hashSync(password, 10);
        User.create({ name: name, surname: surname, email: email, password: hashPassword }).
        then(
            () => {return response.status(201).json({message: "Registration successfully."})}
        );
    } catch (err) {
        response.status(500).json({ message: "Something went wrong" })
    }
}

export { registrationSql }