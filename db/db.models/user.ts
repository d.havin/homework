import Sequelize from "sequelize";
import { sequelize } from "../../src/index";

interface IUser {
  name: string;
  surname: string;
  email: string;
  password: string;
  avatar: string;   
}

const User = sequelize.define("user", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    surname: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    avatar: {
        type: Sequelize.STRING,
        defaultValue: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAurS13TnP6GUZntGWYSu7qZFYO0dUB8rnBg&usqp=CAU"
    }
});

export { User, IUser }