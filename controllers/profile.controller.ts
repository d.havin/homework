import { Request, Response} from 'express';
import { User, IUser } from '../models/user.model';

interface CustomRequest<T> extends Request {
    body: T
} 

const changeAvatar = async function (request: CustomRequest<IUser>, response: Response) {
        try {
            console.log("hello ava")
            console.log(request.body)
            if (request.body.avatar.trim().length == 0){
                return response.status(400).json({message: "please, write avatar URL"});
            } 
            await User.updateOne({_id: request.user._id}, {avatar: request.body.avatar})
            let updatedUser = await User.findOne({_id: request.user._id})
            console.log(updatedUser)
            return response.status(201).json({updatedUser})
        } catch (err) {
            console.log(err)
            response.status(500).json({message: "Something went wrong. Cant change avatar."})
        }
}

// добваить опций

export { changeAvatar }