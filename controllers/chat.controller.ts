import { User, IUser } from "../models/user.model";
import { Chat } from "../models/chat.model";
import { Message } from "../models/message.model";
import { Request, Response } from "express";

interface CustomRequest<T> extends Request {
    body: T;
  }

const createChat = async ( request: Request, response: Response) => {
    try {
        const user = await User.findById({_id: request.body.myId})
        const participant = await User.findById({_id: request.body.canId})
        if (user && participant) {
            let chat = await Chat.findOne({chatParticipants: { $all: [user._id, participant._id]}})
            if (chat) {
                return response.status(201).json({chat})
            } else {
                const chat = await Chat.create({chatParticipants: [user._id, participant._id]})
                // let chat = await Chat.findOne({chatParticipants: { $all: [user._id, participant._id]}})
                if (chat) {
                    user.chats.push(chat._id);
                    await user.save();
                    participant.chats.push(chat._id);
                    await participant.save();
                    return response.status(201).json({chat})
                }
            } 
        }
    } catch (error) {
        response.status(400).send(error)
    }
};

const findMyChat = async (  request: CustomRequest<IUser>, response: Response) => {
    try {
        const chat = await Chat.findById(request.body)
        response.json({chat})
    } catch (error) {
        response.status(400).send(error)
    }
};

const sendMessage = async (message: any) => {
    try {
        const newMessage = await Message.create({messageSendler: message.myId , text: message.message });
        const chat = await Chat.findById(message.chatId)
        if (chat) {
            chat.messages.push(newMessage._id)
            await chat.save()
            return ({chat})
        }
    } catch (error) {
        console.log(error)
    }
};

export {
    findMyChat, 
    sendMessage,
    createChat
}
