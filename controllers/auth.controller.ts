import { User, IUser } from '../models/user.model'
import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken';
import { validationResult } from "express-validator";
import bcrypt from 'bcryptjs'

const accessTokenSecret = 'youraccesstokensecret';
const generateAccessToken = (email: string, _id: string) => {
    const payload = {
        email,
        _id
    }
    return jwt.sign(payload, accessTokenSecret, { expiresIn: '240h' })
}

interface CustomRequest<T>extends Request {
    body: T
} 

const registration = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        const errors: any = validationResult(request)
        if (!errors.isEmpty()) {
            let err = errors;
            return response.status(400).send(` error message: ${err.errors[0].msg}`)
        }
        const { name, surname, email, password} = request.body
        let user = await User.findOne({ email });
        if (user) {
            return response.status(400).json({message: "This email is already in use"})
        }
        const hashPassword = bcrypt.hashSync(password, 10)
        const newUser = await new User({ name: name, surname: surname, email: email, password: hashPassword });
        await newUser.save();
            return response.status(201).json({message: "Registration successfully."});
    } catch (err) {
        response.status(500).json({ message: "Something went wrong" })
    }
}

const authorization = async function (request: CustomRequest<IUser>, response: Response) {
    try {const {email, password} = request.body
    const user = await User.findOne({ email });
    if (!user) {
        return response.status(400).json({ message: `User with such ${email} is not founded` });
    }
    const validPassword = bcrypt.compareSync(password, user.password)
    if (!validPassword) {
        return response.status(400).json({ message: `Wrong password` });
    }
    const token = generateAccessToken(user.email, user._id)
    return response.status(200).json({token: `Bearer ${token}`, user: user})
    } catch (err){

        response.status(500).json({ message: "Somthing went wrong" })
    }
}

const authMiddleware  = (request: CustomRequest<IUser>, response: Response, next: NextFunction) => {
    try {
        const authHeader = request.headers.authorization;
        if (authHeader){
            const token = authHeader.split(' ')[1];
            if (!token){
                return response.status(403).json({message: "User is not logged in"})
            }
            const user = jwt.verify( token, accessTokenSecret )
            request.user = user as IUser
            next()
        }
    }
    catch (err) {
        return response.status(403).json({message: "User is not logged in"})
    }
}

export { registration, authorization, authMiddleware  }