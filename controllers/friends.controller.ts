   import { User, IUser } from "../models/user.model";
import { Request, Response } from "express";

interface CustomRequest<T> extends Request {
  body: T;
}

const getAllUsers = async function (
  request: CustomRequest<IUser>,
  response: Response
) {
  try {
    const users = await User.find({});
    if (!users) {
      return response.status(500).json({ message: "There is nobody here" });
    } else {
      return response.json({ users });
    }
  } catch (err) {
    return response.status(500).json({ message: "Something went wrong" });
  }
};
const createFriendshipRequest = async function (
  request: CustomRequest<IUser>,
  response: Response
) {
  try {
    const user = await User.findOne({ _id: request.user._id });
    if (!user) {
      return response
        .status(500)
        .json({ message: "Something went wrong. Cant find user" });
    }
    const candidate = await User.findOne({ _id: request.body._id });
    if (!candidate) {
      return response.status(500).json({ message: "Something went wrong" });
    }
    user.friendshipRequests.push(candidate._id);
    candidate.invitations.push(user._id);
    await candidate.save();
    await user.save();
    const friendsShipRequests = user.friendshipRequests 
    const users = await User.find({});
    return response.json({ users: users, friendsShipRequests: friendsShipRequests });
  } catch (err) {
    return response
      .status(500)
      .json({ message: "Something went wrong. Cant find candidate" });
  }
};
const cancelFriendshipRequest = async function (
  request: CustomRequest<IUser>,
  response: Response
) {
  try {
    const user = await User.findOne({ _id: request.user._id });
    if (!user) {
      return response
        .status(500)
        .json({ message: "Something went wrong. Cant find user" });
    }
    const candidate = await User.findOne({ _id: request.body._id });
    if (!candidate) {
      return response.status(500).json({ message: "Something went wrong" });
    }
    await User.updateOne(
      { _id: request.user._id },
      { $pull: { friendshipRequests: request.body._id } }
    );
    await User.updateOne(
      { _id: request.body._id },
      { $pull: { invitations: request.user._id } }
    );
    await candidate.save();
    await user.save();
    const users = await User.find({});
    const newUser = await User.findOne({ _id: request.user._id });
    const friendsShipRequests = newUser?.friendshipRequests
    return response.json({ users: users, friendsShipRequests: friendsShipRequests });
  } catch (err) {
    return response.status(500).json({ message: "Something went wrong" });
  }
};
const applyInvitation = async function (  request: CustomRequest<IUser>,  response: Response) {
  try {
    const user = await User.findOne({ _id: request.user._id });
    if (!user) {
      return response
        .status(500)
        .json({ message: "Something went wrong. Cant find user" });
    }
    const candidate = await User.findOne({ _id: request.body._id });
    if (!candidate) {
      return response.status(500).json({ message: "Something went wrong" });
    }
    await User.updateOne(
      { _id: request.user._id },
      { $pull: { invitations: request.body._id } }
    );
    await User.updateOne(
      { _id: request.body._id },
      { $pull: { friendshipRequests: request.user._id } }
    );
    user.friends.push(candidate._id);
    candidate.friends.push(user._id);
    await candidate.save();
    await user.save();
    const users = await User.find({});
    const newUser = await User.findOne({ _id: request.user._id });
    const invitationsList = newUser?.invitations
    const friends = newUser?.friends
    return response.json({ users: users, invitations: invitationsList, friends: friends });
  } catch (err) {
    return response.status(500).json({ message: "Something went wrong" });
  }
};
const rejectInvitation = async function (
  request: CustomRequest<IUser>,
  response: Response
) {
  try {
    const user = await User.findOne({ _id: request.user._id });
    if (!user) {
      return response
        .status(500)
        .json({ message: "Something went wrong. Cant find user" });
    }
    const candidate = await User.findOne({ _id: request.body._id });
    if (!candidate) {
      return response.status(500).json({ message: "Something went wrong" });
    }
    await User.updateOne(
      { _id: request.user._id },
      { $pull: { invitations: request.body._id } }
    );
    await User.updateOne(
      { _id: request.body._id },
      { $pull: { friendshipRequests: request.user._id } }
    );
    await candidate.save();
    await user.save();
    const users = await User.find({});
    const updatedUser = await User.findOne({ _id: request.user._id });
    const invitationsList = updatedUser?.invitations
    return response.json({ users: users, invitations: invitationsList });
  } catch (err) {
    return response.status(500).json({ message: "Something went wrong" });
  }
};
const removeFriend = async function (
  request: CustomRequest<IUser>,
  response: Response
) {
  try {
    const user = await User.findOne({ _id: request.user._id });
    if (!user) {
      return response
        .status(500)
        .json({ message: "Something went wrong. Cant find user" });
    }
    const candidate = await User.findOne({ _id: request.body._id });
    if (!candidate) {
      return response.status(500).json({ message: "Something went wrong" });
    }

    await User.updateOne(
      { _id: request.user._id },
      { $pull: { friends: request.body._id } }
    );
    await User.updateOne(
      { _id: request.body._id },
      { $pull: { friends: request.user._id } }
    );
    await candidate.save();
    await user.save();
    const users = await User.find({});
    const newUser = await User.findOne({ _id: request.user._id });
    const friendsList = newUser?.friends
    return response.json({ users: users, friendsList: friendsList });
  } catch (err) {
    return response.status(500).json({ message: "Something went wrong" });
  }
};
const getAllInvitations = async function (
  request: CustomRequest<IUser>,
  response: Response
) {
  try {
    const user = await User.findOne({ _id: request.user._id });
    if (!user) {
      return response
        .status(500)
        .json({ message: "Something went wrong. Cant find user" });
    }
    const users = user.invitations;
    return response.json({ users });
  } catch (err) {
    return response.status(500).json({ message: "Something went wrong" });
  }
};
const getAllRequests = async function (
  request: CustomRequest<IUser>,
  response: Response
) {
  try {
    const user = await User.findOne({ _id: request.user._id });
    if (!user) {
      return response
        .status(500)
        .json({ message: "Something went wrong. Cant find user" });
    }
    const users = user.friendshipRequests;
    return response.json({ users });
  } catch (err) {
    return response.status(500).json({ message: "Something went wrong" });
  }
};
const getAllFriends = async function (
  request: CustomRequest<IUser>,
  response: Response
) {
  try {
    const user = await User.findOne({ _id: request.user._id });
    if (!user) {
      return response
        .status(500)
        .json({ message: "Something went wrong. Cant find user" });
    }
    const users = user.friends;
    return response.json({ users });
  } catch (err) {
    return response.status(500).json({ message: "Something went wrong" });
  }
};

export {
  getAllUsers,
  getAllFriends,
  getAllInvitations,
  getAllRequests,

  createFriendshipRequest,
  cancelFriendshipRequest,
  applyInvitation,
  rejectInvitation,
  removeFriend,
};
