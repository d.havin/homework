import express from 'express';
import { check } from 'express-validator';
import { registration, authorization, authMiddleware} from '../controllers/auth.controller';
// import { registrationSql } from '../sqlControllers/auth';

const authRouter = express.Router();

authRouter.post("/registration", [
    check('email', "email field cannot be empty").notEmpty(),
    check('password', "password length must be between 4 and 20 symbols").isLength({min:4, max: 20})
], registration);
authRouter.post("/login", authorization);

// authRouter.get('/auth', authMiddleware, getUser);

export { authRouter }