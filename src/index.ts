import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import cors from "cors";
import { authMiddleware } from "../controllers/auth.controller";
import { authRouter } from "../routers/auth.router";
import { profileRouter } from "../routers/profile.router"
import { postRouter } from "../routers/post.router";
import { friendsRouter } from '../routers/friend.router'
import { findNewFriendsRouter } from '../routers/findNewFriends.router';
import { chatRouter } from "../routers/chat.router";
import { sendMessage } from "../controllers/chat.controller"
import { Server } from "socket.io"

const Sequelize = require("sequelize");
// import Sequelize from "sequelize";

require('dotenv').config()
const app = express();
const PORT = 3050;
const Stripe = require('stripe')
const stripe = Stripe(process.env.STRIPE_SECRET_KEY)

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(
  cors({
    origin: "http://localhost:3000",                                // "", [], boolean, function (origin, callback) {}
    // methods: ["GET, POST"],                                          Настраивает заголовок  CORS Access-Control-Allow-Methods
    // allowedHeaders: ['Content-Type', 'Authorization']                Настраивает заголовок CORS Access-Control-Allow-Headers
    // credentials: true                                                Передача куков
    // maxAge: 60                                                       Настраивает заголовок CORS Access-Control-Max-Age     в секундах   
  })
);            // app.options('/', cors())  Включение CORS Pre-Flight

app.use("/auth", authRouter);
app.use("/profile", authMiddleware, profileRouter);
app.use("/posts", authMiddleware, postRouter);
app.use("/friends", authMiddleware, friendsRouter);
app.use("/findnewfriends", authMiddleware, findNewFriendsRouter);
app.use("/chats", authMiddleware, chatRouter);
app.post("/payment", async (req, res) => {
  let { amount, id } = req.body
  try {
    const payment = await stripe.paymentIntents.create({
      amount,
      currency: "USD",
      description: "Evil company",
      payment_method: id,
      confirm: true
    })
    res.json({
      massage: "Payment successful",
      success: true
    })
  } catch (error) {
    res.json({
      error,
      message: error,
      success: false
    })
  }
})

const sequelize = new Sequelize("network", "postgres", "Gawin2909", {
  dialect: "postgres",
  host: "localhost",
  port: 3050,
  logging: false,
  define: {timestamps: false}
});

const start = async () => {
  try {
    await mongoose.connect(
      "mongodb://localhost:27017/socialNetwork",
      {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
      },
      () => {
        console.log("Connection to mongoDB has been established successfully.");
      }
    );
    try {
      await sequelize.authenticate();
      console.log('Connection to postgresSql has been established successfully.');
    } catch (error) {
      console.error('Unable to connect to the database:', error);
    }

    sequelize.sync().then(()=>{
      const server = app.listen(PORT, () => console.log(`The application is listening on port ${PORT}`))
      
      const io = new Server(server)
      io.on("connection", (socket) => {
        socket.on('sendMessage', async (message) => {
          const messages = await sendMessage(message)
          io.emit('tweet', messages);
        })
      })
    }).catch((err:any)=> console.log(err));

  } catch (err) {
    console.log(err)
  }
}

start();

export { sequelize }